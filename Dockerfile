FROM node:13-buster-slim
EXPOSE 3000
EXPOSE 19000
EXPOSE 19001
EXPOSE 19002
ENV ADB_IP="192.168.1.1"
ENV REACT_NATIVE_PACKAGER_HOSTNAME="192.168.1.1"
RUN apt-get -y update
RUN apt-get install -y android-tools-adb
RUN apt-get install -y git
WORKDIR /app
COPY package.json /app
COPY app.json /app
RUN npm install -g expo-cli react-native-cli
RUN npm install
COPY . /app
CMD adb connect $ADB_IP
CMD [ "npm", "start" ]
